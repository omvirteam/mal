function show_notify(notify_msg,notify_type)
    {
        if(notify_type == true){
            $.growl.notice({ title:"Success!",message:notify_msg});
        }else{
            $.growl.error({ title:"False!",message:notify_msg});
        }
    }
    
    /**
	 * @param $selector
	 * @constructor
     */
    function initAjaxSelect2($selector,$source_url)
    {
        $selector.select2({
            placeholder: " --Select-- ",
            allowClear: true,
            width:"100%",
            ajax: {
                url: $source_url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data,params) {
                    params.page = params.page || 1;
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * 5) < data.total_count
                        }
                    };
                },
                cache: true
            }
        });
    }

    function setSelect2Value($selector,$source_url = '')
    {
        if($source_url != '') {
            $.ajax({
                url: $source_url,
                type: "GET",
                data: null,
                contentType: false,
                cache: false,
                async: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $selector.empty().append($('<option/>').val(data.id).text(data.text)).val(data.id).trigger("change");
                    }
                }
            });
        } else {
            $selector.empty().append($('<option/>').val('').text('--select--')).val('').trigger("change");
        }
    }
    function setSelect2MultiValue($selector,$source_url = '')
    {
        if($source_url != '') {
            $.ajax({
                url: $source_url,
                type: "GET",
                data: null,
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
						var selectValues = data[0];
						$.each(selectValues, function(key, value) {   
							$selector.select2("trigger", "select", {
								data: value
							});
						});
                    }
                }
            });
        } else {
            $selector.empty().append($('<option/>').val('').text('--select--')).val('').trigger("change");
        }
    }
    //Tags
    function initAjaxSelect2Tags($selector,$source_url)
    {
        $selector.select2({
            placeholder: " --Select-- ",
            allowClear: true,
            width:"100%",
            tags: true,
            multiple: true,
            maximumSelectionLength: 1,
            ajax: {
                url: $source_url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data,params) {
                    params.page = params.page || 1;
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * 5) < data.total_count
                        }
                    };
                },
                cache: true
            }
        });
    }

