<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employee extends CI_Controller
{
    public $logged_in_id = null;
    public $now_time = null;

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Appmodel', 'app_model');
        $this->load->model('Crud', 'crud');
        $this->now_time = date('Y-m-d H:i:s');
        if (!$this->session->userdata('is_logged_in')) {
            redirect('');
        }
        $this->logged_in_id = $this->session->userdata('is_logged_in')['admin_id'];
        $this->now_time = date('Y-m-d H:i:s');
    }
    function add_employee($employee_id = ''){
        $data = array();
        if (isset($employee_id) && !empty($employee_id)) {
            $employee_data = $this->crud->get_row_by_id('employee', array('employee_id' => $employee_id));
            $employee_data = $employee_data[0];
            $data['employee_data'] = $employee_data;
          // echo '<pre>'; print_r($data); exit;
        }
		set_page('employee/add_employee',$data);
	}
    function save_employee() {
        $post_data = $this->input->post();
        if (isset($post_data['employee_id']) && !empty($post_data['employee_id'])) {
            $post_data['updated_by'] = $this->logged_in_id;
            $where_array['employee_id'] = $post_data['employee_id'];
            $result = $this->crud->update('employee', $post_data, $where_array);
            if ($result) {
                $return['success'] = "Updated";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'employee Updated Successfully');
            }
        } else {
            $post_data['created_at'] = $this->now_time;
            $post_data['created_by'] = $this->logged_in_id;
            $result = $this->crud->insert('employee', $post_data);
            if ($result) {
                $return['success'] = "Added";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Employee Added Successfully');
            }
        }
        print json_encode($return);
        exit;
    }
    function employee_list(){
		set_page('employee/employee_list');
	}
    function employee_list_datatable() {

        $post_data = $this->input->post();
        $config['table'] = 'employee e';
        $config['select'] = 'e.*';
        $config['column_search'] = array('e.emp_name','e.dept','e.pf_applicable','e.esic_applicable','e.basic_salary','e.basic_wage','e.hra','e.pay_type','e.conveyance','e.special_allowance','e.total_salary','e.earn_leave_salary','e.bonus','e.gratuity');
        $config['column_order'] = array(null,'e.emp_name','e.dept','e.pf_applicable','e.esic_applicable','e.basic_salary','e.basic_wage','e.hra','e.pay_type','e.conveyance','e.special_allowance','e.total_salary','e.earn_leave_salary','e.bonus','e.gratuity');
        $config['order'] = array('e.employee_id' => 'desc');
        $this->load->library('datatables', $config, 'datatable');
        $list = $this->datatable->get_datatables();
        $data = array();
        //echo '<pre>'; print_r($list); exit;
        foreach ($list as $employee) {
            $row = array();
            $action = '';
            $action .= '<a href="javascript:void(0);" class="delete_button" data-href="' . base_url('demo/delete/' . $employee->employee_id) . '"><span class="glyphicon glyphicon-trash" style="color : red">&nbsp;</span></a>';
            $row[] = $action;
            $row[] = $employee->emp_name;
            $row[] = $employee->dept;
            $row[] = $employee->pf_applicable;
            $row[] = $employee->esic_applicable;
            $row[] = $employee->basic_salary;
            $row[] = $employee->basic_wage;
            $row[] = $employee->hra;
            $row[] = $employee->pay_type;
            $row[] = $employee->conveyance;
            $row[] = $employee->special_allowance;
            $row[] = $employee->earn_leave_salary;
            $row[] = $employee->bonus;
            $row[] = $employee->gratuity;
            $data[] = $row;
            //echo '<pre>'; print_r($row); exit;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatable->count_all(),
            "recordsFiltered" => $this->datatable->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function delete($id = '') {
        $table = $_POST['table_name'];
        $id_name = $_POST['id_name'];
        $this->crud->delete($table, array($id_name => $id));
    }
}
