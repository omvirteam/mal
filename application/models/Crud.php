<?php

/**
 * Class Crud
 * &@property CI_DB_active_record $db
 */
class Crud extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $table_name
	 * @param $data_array
	 * @return bool
	 */
	function insert($table_name,$data_array){
		if($this->db->insert($table_name,$data_array))
		{
			return $this->db->insert_id();
		}
		return false;
	}

	function insertFromSql($sql)
	{
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	function execuetSQL($sql){
		$this->db->query($sql);
	}
	function getFromSQL($sql)
	{
		return $this->db->query($sql)->result();
	}

	/**
	 * @param $table_name
	 * @param $order_by_column
	 * @param $order_by_value
	 * @return bool
	 */
	function get_all_records($table_name,$order_by_column,$order_by_value){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
	 * @param $table_name
	 * @param $order_by_column
	 * @param $order_by_value
	 * @param $where_array
	 * @return bool
	 */
	function get_all_with_where($table_name,$order_by_column,$order_by_value,$where_array)
	{
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_array);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
	 * @param $tbl_name
	 * @param $column_name
	 * @param $where_id
	 * @return mixed
	 */
	function get_column_value_by_id($tbl_name,$column_name,$where_id)
	{				
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where_id);		
		$this->db->last_query();
		$query = $this->db->get();
		return $query->row($column_name);
	}

	/**
	 * @param $table_name
	 * @param $where_id
	 * @return mixed
	 */
	function get_row_by_id($table_name,$where_id){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_id);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @param $table_name
	 * @param $where_array
	 * @return mixed
	 */
	function delete($table_name,$where_array){		
		$result = $this->db->delete($table_name,$where_array);
		return $result;
	}
	
	/**
	 * @param $table_name
	 * @param $where_id
	 * @param $where_in_array
	 * @return mixed
	 */
	function delete_where_in($table_name, $where_id, $where_in_array){		
		$this->db->where_in($where_id, $where_in_array);
		$result = $this->db->delete($table_name);
		return $result;
	}

	/**
	 * @param $table_name
	 * @param $data_array
	 * @param $where_array
	 * @return mixed
	 */
	function update($table_name,$data_array,$where_array){
		$this->db->where($where_array);
		$rs = $this->db->update($table_name, $data_array);
		return $rs;
	}

	/**
	 * @param $name
	 * @param $path
	 * @return bool
	 */
	function upload_file($name, $path)
	{
		$config['upload_path'] = $path;
		$config ['allowed_types'] = '*';
		$this->upload->initialize($config);
		if($this->upload->do_upload($name))
		{
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
		return false;
	}

	/**
	 * @param $table
	 * @param $id_column
	 * @param $column
	 * @param $column_val
	 * @return null
	 */
	function get_id_by_val($table,$id_column,$column,$column_val){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column,$column_val);
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}

	function get_id_by_val_where($table,$id_column,$column,$column_val,$where){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column,$column_val);
		$this->db->where($where);
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}
	
	function get_id_by_val_count($table,$id_column,$column,$column_val){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column,$column_val);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->num_rows();
		} else {
			return null;
		}
	}
	
	function get_id_by_val_not($table,$id_column,$column,$column_val,$permalink){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column,$column_val);
		$this->db->where_not_in($id_column, $permalink);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}
	
	function get_same_by_val($table,$id_column,$column1,$column1_val,$column2,$column2_val,$id = null){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column1,$column1_val);
		$this->db->where($column2,$column2_val);
		$this->db->where($id_column."!=",$id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}

	function get_same_by_multi_where($table,$id_column,$column1,$column1_val,$column2,$column2_val,$column3,$column3_val,$id = null){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column1,$column1_val);
		$this->db->where($column2,$column2_val);
		$this->db->where($column3,$column3_val);
		$this->db->where($id_column."!=",$id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}
	
	function limit_words($string, $word_limit=30){
		$words = explode(" ",$string);
		return implode(" ", array_splice($words, 0, $word_limit));
	}
	function limit_character($string, $character_limit=30){
		if (strlen($string) > $character_limit) {
			return substr($string, 0, $character_limit).'...';
		}else{
			return $string;
		}
	}

	//select data

	function get_select_data($tbl_name)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$query = $this->db->get();
		return $query->result();
	}

	function get_select_data_where($tbl_name,$where,$where1)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where);
		$this->db->where($where1);
		$this->db->order_by("name","asc");
		$query = $this->db->get();
		return $query->result();
	}

	// Select data For specific Columns
	// $columns Array
	function get_specific_column_data($tbl_name, $columns)
	{
		$columns = implode(', ', $columns);
		$this->db->select($columns);
		$this->db->from($tbl_name);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @param $tbl_name
	 * @param $where
	 * @param $where_id
	 * @return mixed
	 */
	function get_data_row_by_id($tbl_name,$where,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where,$where_id);
		$query = $this->db->get();
		return $query->row();
	}
	function get_result_where($tbl_name,$where,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where,$where_id);
		$query = $this->db->get();
		return $query->result();
	}

	function get_where_in_result($tbl_name,$where,$where_in)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where_in($where,$where_in);
		$query = $this->db->get();
		return $query->result();
	}
	
	
	function getUserChatRoleIDS($user_id)
	{
		$array = array();
		$sql = "SELECT * FROM chat_roles WHERE staff_id='$user_id'";
		$rows = $this->db->query($sql)->result();        
		$i = 0;        
		foreach($rows as $row)
		{
			$array[$i] = $row->allowed_staff_id;
			$i++;        
		}    
		return $array;
	}
	
	function get_max_number($tbl_name,$column_name)
	{
		$this->db->select_max($column_name);
		$result = $this->db->get($tbl_name)->row();  
		return $result;
	}
	
	function get_last_record_by_id($table_name, $column_id, $column_val, $order_by_column, $order_by_value){
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($column_id, $column_val);
		$this->db->order_by($order_by_column, $order_by_value);
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}

	function get_last_record_by_where_array($table_name, $where_array, $order_by_column, $order_by_value){
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($where_array);
		$this->db->order_by($order_by_column, $order_by_value);
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_all_parameters(){
		$this->db->select("p.*, c.category_name");
		$this->db->from('parameter p');
		$this->db->join('category c', 'c.category_id = p.category_id');
		$this->db->order_by('c.category_order','ASC');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function get_current_shift($current_time){
		$this->db->select("*");
		$this->db->from('shift');
		$this->db->where(array('start_time <=' => $current_time, 'end_time >=' => $current_time));
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_next_autoincrement($tbl_name)
	{
		$query = $this->db->query('SHOW TABLE STATUS WHERE `Name` = "'.$tbl_name.'"');
		$data = $query->result_array();
		return $data[0]['Auto_increment'];
	}
		
}
