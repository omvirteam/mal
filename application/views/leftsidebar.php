<?php
$segment1 = $this->uri->segment(1);
$segment2 = $this->uri->segment(2);
$segment3 = $this->uri->segment(3);
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
            <li class="<?= ($segment1 == '') ? 'active' : '' ?>">
                <a href="<?= base_url(); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="<?= ($segment1 == 'demo' && $segment2 == 'total_free_leaves') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/total_free_leaves'); ?>">
                    <i class="fa fa-circle"></i> <span>Total Free Leaves</span>
                </a>
            </li>
            <li class="<?= ($segment1 == 'demo' && $segment2 == 'weekly_leaves') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/weekly_leaves'); ?>">
                    <i class="fa fa-circle"></i> <span>Weekly Leaves</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'yearly_leaves') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/yearly_leaves'); ?>">
                    <i class="fa fa-circle"></i> <span>Yeary Leaves</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'employee' && $segment2 == 'add_employee') ? 'active' : '' ?>">
                <a href="<?= base_url('employee/add_employee'); ?>">
                    <i class="fa fa-circle"></i> <span>Add Employee</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'employee' && $segment2 == 'employee_list') ? 'active' : '' ?>">
                <a href="<?= base_url('employee/employee_list'); ?>">
                    <i class="fa fa-circle"></i> <span>Employee List</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'employee_detail') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/employee_detail'); ?>">
                    <i class="fa fa-circle"></i> <span>Employee Detail</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'total_salary') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/total_salary'); ?>">
                    <i class="fa fa-circle"></i> <span>Total Salary</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'salary_detail') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/salary_detail'); ?>">
                    <i class="fa fa-circle"></i> <span>Salary Detail</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'salary_slip') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/salary_slip'); ?>" target="_blank">
                    <i class="fa fa-circle"></i> <span>Salary Slip</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'salary_report') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/salary_report'); ?>">
                    <i class="fa fa-circle"></i> <span>Salary Report</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'leave_master') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/leave_master'); ?>">
                    <i class="fa fa-circle"></i> <span>Leave Master</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'present_hours') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/present_hours'); ?>">
                    <i class="fa fa-circle"></i> <span>Present Hours</span>
                </a>
            </li>
            <li class=" <?= ($segment1 == 'demo' && $segment2 == 'apply_leaves') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/apply_leaves'); ?>">
                    <i class="fa fa-circle"></i> <span>Apply Leave</span>
                </a>
            </li>
             <li class=" <?= ($segment1 == 'demo' && $segment2 == 'department_attendance') ? 'active' : '' ?>">
                <a href="<?= base_url('demo/department_attendance'); ?>">
                    <i class="fa fa-circle"></i> <span> Department Attendance</span>
                </a>
            </li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>

