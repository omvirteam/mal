<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Director List
            <a href="<?= base_url() ?>demo/add_employee" class="btn btn-primary btn-sm pull-right" style="margin: 5px;">Add Employee</a>
        </h1>
    </section>
    <div class="clearfix">
        <div class="row">
            <div style="margin: 15px;">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="box-body table-responsive">
                                        <table id="employee_list" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Employee Name</th>
                                                    <th>Department Name</th>
                                                    <th>PF Applicable</th>
                                                    <th>ESIC Applicable</th>
                                                    <th>Basic Salary</th>
                                                    <th>HRA</th>
                                                    <th>Pay Type</th>
                                                    <th>Conveyance</th>
                                                    <th>Special Allowance</th>
                                                    <th>Total Salary</th>
                                                    <th>Earn Leave Salary</th>
                                                    <th>Bonus</th>
                                                    <th>Gratuity</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        
		table = $('#employee_list').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('demo/employee_list_datatable')?>",
                "type": "POST",
                "data": function(d){
                },
            },
             "scrollY": 450,
            "scroller": {
                "loadingIndicator": true
            },
//            "sScrollX":  "100%",
//            "sScrollXInner":  "110%"
        });

        $(document).on("click", ".delete_button", function () {
            var value = confirm('Are you sure delete this records?');
            var tr = $(this).closest("tr");
            if (value) {
                $.ajax({
                    url: $(this).data('href'),
                    type: "POST",
                    data: 'id_name=employee_id&table_name=employee',
                    success: function (data) {
                        table.draw();
                        show_notify('Employee Deleted Successfully!', true);
                    }
                });
            }
        });
    });
</script>


