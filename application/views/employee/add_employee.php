<div class="content-wrapper">
	<form class="form-horizontal" action="" method="post" id="save_employee" data-parsley-validate="">
		<?php if (isset($employee_data->employee_id) && !empty($employee_data->employee_id)) { ?>
		<input type="hidden" name="employee_id" id="employee_id" value="<?= $employee_data->employee_id ?>">
		<?php } ?>
		<section class="custom content-header">
			<h1>
				<small class="text-primary text-bold">Employee Master : Employee</small>
					<button type="submit" class="btn btn-info btn-xs pull-right module_save_btn pull-right" style="margin: 5px;">Save Employee</button>
                    <a href="<?=base_url('employee/employee_list')?>" class="btn btn-info btn-xs pull-right pull-right" style="margin: 5px;">Employee List</a>
			</h1>
		</section>
		<div class="clearfix">
			
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab">Add Employee</a></li> 
						
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="row"> 
								<div class="col-md-12">
									<fieldset class="scheduler-border">
										<legend class="scheduler-border text-primary text-bold"> Personal Information </legend>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="emp_code" class="col-sm-5 input-sm">Employee Code</label>
													<div class="col-sm-7">
														<input type="text" name="" id="emp_code" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="clearfix"></div>
												
												<div class="form-group">
													<label for="emp_name" class="col-sm-5 input-sm">Employee Name</label>
													<div class="col-sm-7">
														<input type="text" name="emp_name" id="emp_name" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="lic_id" class="col-sm-5 input-sm">LIC ID</label>
													<div class="col-sm-7">
                                                        <input type="text" id="lic_id" class="form-control input-sm" >
													</div>
												</div>
												<div class="clearfix"></div>
												
												<div class="form-group">
													<label for="unit_depart" class="col-sm-5 input-sm">Unit (Department)</label>
													<div class="col-sm-7">
														<select name="dept" id="dept" class="form-control input-sm">
															<option>d1</option>
															<option>d2</option>
															<option>d3</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="designation" class="col-sm-5 input-sm">Designation</label>
													<div class="col-sm-7">
														<select name="" id="designation" class="form-control input-sm">
															<option>d1</option>
															<option>d2</option>
															<option>d3</option>
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												
												<div class="form-group">
													<label for="pf_no" class="col-sm-5 input-sm">PF No.</label>
													<div class="col-sm-7">
														<input type="text" name="" id="pf_no" class="form-control input-sm input-datepicker" value="<?= (isset($employee_data->birth_date)) ? date('d-m-Y', strtotime($employee_data->birth_date)) : ''; ?>" >
													</div>
												</div>
												<div class="form-group">
													<label for="uan_no" class="col-sm-5 input-sm">UAN No.</label>
													<div class="col-sm-7">
														<input type="text" name="" id="uan_no" class="form-control input-sm" value="<?= (isset($employee_data->qualification)) ? $employee_data->qualification : ''; ?>" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
                                                    <label for="esic_no" class="col-sm-5 input-sm">ESIC No.</label>
													<div class="col-sm-7">
														<input type="text" name="" id="esic_no" class="form-control input-sm" value="<?= (isset($employee_data->father_name)) ? $employee_data->father_name : ''; ?>" >
													</div>
												</div>
												<div class="form-group">
													<label for="pan_no" class="col-sm-5 input-sm">PAN</label>
													<div class="col-sm-7">
														<input type="text" name="" id="pan_no" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="aadhar_no" class="col-sm-5 input-sm">Aadhaar</label>
													<div class="col-sm-7">
														<input type="text" name="" id="aadhar_no" class="form-control input-sm num-only" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="mobile_no" class="col-sm-5 input-sm">Mobile No.</label>
													<div class="col-sm-7">
														<input type="text" name="" id="mobile_no" class="form-control input-sm num-only" value="<?= (isset($employee_data->husband_mobile)) ? $employee_data->husband_mobile : ''; ?>" >
													</div>
												</div>
												<div class="form-group">
													<label for="birth_date" class="col-sm-5 input-sm">Birth Date</label>
													<div class="col-sm-7">
														<input type="text" name="" id="datepicker1" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="joining_date" class="col-sm-5 input-sm">Joining Date</label>
													<div class="col-sm-7">
														<input type="text" name="" id="datepicker2" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="resign_date" class="col-sm-5 input-sm">Resign Date</label>
													<div class="col-sm-7">
														<input type="text" name="" id="datepicker2" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="pf_applicable" class="col-sm-5 input-sm">PF Applicable</label>
													<div class="col-sm-7">
														<select name="pf_applicable" id="pf_applicable" class="form-control input-sm">
															<option>Yes</option>
															<option>No</option>
															
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="esic_applicable" class="col-sm-5 input-sm">ESIC Applicable</label>
													<div class="col-sm-7">
														<select name="esic_applicable" id="esic_applicable" class="form-control input-sm">
															<option>Yes</option>
															<option>No</option>
															
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label for="basic_salary" class="col-sm-5 input-sm">Basic Salary</label>
													<div class="col-sm-7">
														<input type="text" name="basic_salary" id="basic_salary" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="basic_wage" class="col-sm-5 input-sm">Basic Wage</label>
													<div class="col-sm-7">
														<input type="text" name="basic_wage" id="basic_wage" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="hra" class="col-sm-5 input-sm">HRA</label>
													<div class="col-sm-7">
														<input type="text" name="hra" id="hra" class="form-control input-sm" value="" >
													</div>
												</div>
												
											</div>
											<div class="col-md-6">
												<table class="table">
													<thead>
													<tr>
														<th></th>
														<th>Name</th>
														<th>Aadhar</th>
														<th>Birth Date</th>
													
													</tr>
													</thead>
													<tbody>													
														<tr class="odd gradeX">
															<td>Mother</td>
                                                            <td><input type="text" name="" id="mother_name" class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="mother_aadhar" class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="mother_bdate" class="form-control input-sm" value="" ></td>
														</tr>
														<tr class="odd gradeX">
															<td>Father</td>
                                                            <td><input type="text" name="" id="father_name" class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="father_aadhar" class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="father_bdate" class="form-control input-sm" value="" ></td>
															
														</tr>
														<tr class="odd gradeX">
															<td>Wife</td>
                                                            <td><input type="text" name="" id="wife_name" class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="wife_aadhar" class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="wife_bdate" class="form-control input-sm" value="" ></td>
															
														</tr>
														<tr class="odd gradeX">
															<td>Children</td>
                                                            <td><input type="text" name="" id="children_name"  class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="children_name" class="form-control input-sm" value="" ></td>
                                                            <td><input type="text" name="" id="children_bdate" class="form-control input-sm" value="" ></td>
														</tr>
													</tbody>
												</table>
												<div class="form-group">
													<label for="bank_name" class="col-sm-5 input-sm">Bank Name</label>
													<div class="col-sm-7">
														<input type="text" name="" id="bank_name" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="bank_ac_name" class="col-sm-5 input-sm">Bank A/c Name</label>
													<div class="col-sm-7">
														<input type="text" name="" id="bank_ac_name" class="form-control input-sm" value="">
													</div>
												</div>
												<div class="form-group">
													<label for="bank_ac_no" class="col-sm-5 input-sm">Bank A/c No.</label>
													<div class="col-sm-7">
                                                        <input type="text" name="" id="bank_ac_no" class="form-control input-sm">
													</div>
												</div>
												<div class="form-group">
													<label for="ifsc_code" class="col-sm-5 input-sm">IFSC Code</label>
													<div class="col-sm-7">
														<input type="text" name="" id="ifsc_code" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="pay_type" class="col-sm-5 input-sm">Pay Type</label>
													<div class="col-sm-7">
														<select name="pay_type" id="pay_type" class="form-control input-sm">
															<option>Fix</option>
															<option>Wage</option>
														</select>
													</div>
												</div>
                                                <div class="form-group">
													<label for="conveyance" class="col-sm-5 input-sm">Conveyance</label>
													<div class="col-sm-7">
														<input type="text" name="conveyance" id="conveyance" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="special_allowance" class="col-sm-5 input-sm">Special Allowance</label>
													<div class="col-sm-7">
														<input type="text" name="special_allowance" id="special_allowance" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="total_salary" class="col-sm-5 input-sm">Total Salary</label>
													<div class="col-sm-7">
														<input type="text" name="total_salary" id="total_salary" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="earn_leave_salary" class="col-sm-5 input-sm"> Earn Leave Salary</label>
													<div class="col-sm-7">
														<input type="text" name="earn_leave_salary" id="earn_leave_salary" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="bonus" class="col-sm-5 input-sm">Bonus</label>
													<div class="col-sm-7">
														<input type="text" name="bonus" id="bonus" class="form-control input-sm" value="" >
													</div>
												</div>
												<div class="form-group">
													<label for="gratuity" class="col-sm-5 input-sm">Gratuity</label>
													<div class="col-sm-7">
														<input type="text" name="gratuity" id="gratuity" class="form-control input-sm" value="" >
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        
		$(document).on('submit', '#save_employee', function () {
           
            $('.module_save_btn').attr('disabled', 'disabled');
			var postData = new FormData(this);
			$.ajax({
				url: "<?=base_url('demo/save_employee') ?>",
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function (response) {
					var json = $.parseJSON(response);
					
					if (json['success'] == 'Added'){
						window.location.href = "<?php echo base_url('employee/employee_list') ?>";
					}
					if (json['success'] == 'Updated'){
						window.location.href = "<?php echo base_url('employee/employee_list') ?>";
					}
					return false;
				},
			});
			return false;
		});

	});
   
</script>
	
