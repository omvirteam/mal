<!-- Main Footer -->
<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds.</p>
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2018 <a href="<?=base_url()?>">Mal</a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->
<!-- REQUIRED JS SCRIPTS -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="<?=base_url();?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?=base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=base_url();?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url();?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?=base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?=base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?=base_url();?>assets/dist/js/demo.js"></script>

<!-------- /Parsleyjs --------->
<script src="<?= base_url('assets/plugins/parsleyjs/dist/parsley.min.js');?>"></script>

<!-- notify -->
<script src="<?php echo base_url('assets/plugins/notify/jquery.growl.js');?>"></script>
<!-- DataTables -->
<script src="<?=base_url('assets/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js');?>"></script>
<script src="<?=base_url();?>assets/plugins/datatables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>
<script>
	$(document).ready(function(){
		$('#datepicker1,#datepicker2,#datepicker3').datepicker({
			format: 'dd-mm-yyyy',
			todayBtn: "linked",
			todayHighlight: true,
			autoclose: true
		});
        
       $(document).on('input',".num_only",function(){
            this.value = this.value.replace(/[^\d\.\-]/g,'');
        });
		
	});
</script>
</body>
</html>