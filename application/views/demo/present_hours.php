<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Present Hours
        </h1>
    </section>
    <div class="clearfix">
        <div class="row">
            <div style="margin: 15px;">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <form class="frm-add-real-leave" method="post">
                                    <div class="panel-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-4 input-sm">Select Employee</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control input-sm disabled" name="employee_id">
                                                        <option>Select</option>
                                                        <option>Administrator</option>
                                                        <option>Arva Selecon</option>
                                                        <option>Mona Sakhiya</option>
                                                        <option>Root</option>
                                                        <option>Test Emp</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-4 input-sm">Present Hours</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="present_hours" id="present_hours" class="form-control input-sm num-only">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-sm-4 input-sm">Present Date</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="datepicker1" class="form-control input-sm">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-9">
                                        </div>
                                        <div class="col-md-3">
                                            <div class="col-md-7 pull-right">
                                                <div class="form-group">
                                                    <button class="btn btn-primary btn-xs btn-block btn-add-real-leave" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Of Employee Present Hours
        </h1>
    </section>
    <div class="clearfix">
        <div style="margin: 15px;">
            <!-- Horizontal Form -->
            <div class="box box-primary">
                <div class="box-body">
                    <div class="col-md-12">
                        <form name="report-filter-form" method="post">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-sm-5 input-sm">Select Employee</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm" name="employee_id" id="filter-employee">
                                            <option>Select</option>
                                            <option>Administrator</option>
                                            <option>Arva Selecon</option>
                                            <option>Mona Sakhiya</option>
                                            <option>Root</option>
                                            <option>Test Emp</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-sm-5 input-sm">Select Month</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm select2" name="month" id="filter-month">
                                            <option value="all">All</option>
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-sm-5 input-sm">Select Year</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm select2" name="month" id="filter-year">
                                            <option value="all">All</option>
                                            <option value="1">2017-18</option>
                                            <option value="2">2018-19</option>
                                            <option value="3">2019-20</option>
                                            <option value="4">2020-21</option>
                                            <option value="5">2021-22</option>
                                            <option value="6">2022-23</option>
                                            <option value="7">2023-24</option>
                                            <option value="8">2024-25</option>
                                            <option value="9">2025-26</option>
                                            <option value="10">2026-27</option>
                                            <option value="11">2027-28</option>
                                            <option value="12">2028-29</option>
                                            <option value="12">2029-30</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br/>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="table-responsive">
                        <table class="padding-td-th-5px table table-bordered" id="table-leave">
                            <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>01</th>
                                    <th>02</th>
                                    <th>03</th>
                                    <th>04</th>
                                    <th>05</th>
                                    <th>06</th>
                                    <th>07</th>
                                    <th>08</th>
                                    <th>09</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <th>19</th>
                                    <th>20</th>
                                    <th>21</th>
                                    <th>22</th>
                                    <th>23</th>
                                    <th>24</th>
                                    <th>25</th>
                                    <th>26</th>
                                    <th>27</th>
                                    <th>28</th>
                                    <th>29</th>
                                    <th>30</th>
                                    <th>31</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>January</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>February</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>March</td>
                                   <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>April</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>  
                                </tr>
                                <tr>
                                    <td>May</td>
                                     <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>June</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>July</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>August</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>September</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>October</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>Navember</td>
                                   <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>December</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                    <td>7</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>6</td>
                                    <td>8</td>
                                    <td>6</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
    var table = $('#table-leave').DataTable({
    "processing": true,
            "serverSide": true,
            "order": [],
            "ordering": false,
            "bPaginate": false,
            "bFilter": false,
    });
            $('#leave_date').daterangepicker({
    "locale": {
    "format": "DD-MM-YYYY"
    }
    });
            $('#leave_date').on('apply.daterangepicker', function (ev, picker) {
    $('#from_date').val(picker.startDate.format('DD-MM-YYYY'));
            $('#to_date').val(picker.endDate.format('DD-MM-YYYY'));
    });
            $("#filter-year").datepicker({
    autoclose: true,
            format: "yyyy",
            startView: 2,
            minViewMode: 2,
            maxViewMode: 2
    });
            $(document).on('change', '#filter-employee,#filter-month,#filter-year', function(){
    table.draw();
    });
    }
    );
    }
    );
</script>
