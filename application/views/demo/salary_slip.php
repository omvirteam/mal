<html>
    <head>
		<title>Salary Slip | Mal</title>
		<style>
			@media print {
				table{
					border-spacing: 0;
				}
				.text-center {
					text-align: center !important;
				}
				.text-right {
					text-align: right !important;
				}
				.text-left {
					text-align: left !important;
				}
				.text-underline {
					text-decoration: underline;
				}
				.section-title{
					font-size: 20px;
					font-weight: 900;
					margin: 15px 10px;
				}
				.no-margin{
					margin: 0;
				}
				table.table-item-detail > thead > tr > th:last-child{
					border-right: solid 0.5px #000000;
				}
				table.table-item-detail > thead > tr > th:first-child{
					border-left: solid 0.5px #000000;
				}
				table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
					border-right: solid 0.5px #000000;
				}
				table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
					border-left: solid 0.5px #000000;
				}
				table.table-item-detail > thead > tr > th{
					border-top: solid 0.5px #000000;
					border-bottom: solid 0.5px #000000;
				}
				table.table-item-detail > tbody > tr > th{
					border-top: solid 0.5px #000000;
					border-bottom: solid 0.5px #000000;
				}
			}
			table.table-item-detail > thead > tr > th:last-child{
				border-right: solid 0.5px #000000;
			}
			table.table-item-detail > thead > tr > th:first-child{
				border-left: solid 0.5px #000000;
			}
			table.table-item-detail > tbody > tr > th:last-child,table.table-item-detail > tbody > tr > td:last-child{
				border-right: solid 0.5px #000000;
			}
			table.table-item-detail > tbody > tr > th:first-child,table.table-item-detail > tbody > tr > td:first-child{
				border-left: solid 0.5px #000000;
			}
			table.table-item-detail > thead > tr > th{
				border-top: solid 0.5px #000000;
				border-bottom: solid 0.5px #000000;
			}
			table.table-item-detail > tbody > tr > th{
				border-top: solid 0.5px #000000;
				border-bottom: solid 0.5px #000000;
			}
			table{
				border-spacing: 0;
			}
			table tr td {
				padding-left: 5px;
				padding-right: 5px;
			}
			.text-center {
				text-align: center !important;
			}
			.text-right {
				text-align: right !important;
			}
			.text-left {
				text-align: left !important;
			}
			.text-underline {
				text-decoration: underline;
			}
			.section-title{
				font-size: 20px;
				font-weight: 900;
				margin: 15px 10px;
			}
			.no-margin{
				margin: 0;
			}
			.content {
				padding: 10px;
			}
			.no-border-top{
				border-top:0;
			}
			.no-border-bottom{
				border-bottom:0 !important;
			}
			.no-border-left{
				border-left:0;
			}
			.no-border-right{
				border-right:0;
			}
			.no-border {
				border: 0;
			}
			.border-right {
				border-right: 1px solid black;
				border-left: 0;
				border-top: 0;
				border-bottom: 0;
			}
            .line{
                
                border-top: 1px solid black;
                position: absolute;;
            }

		</style>
	</head>
	<body>
		<table class="table-company-detail" border="0" style="font-size: 13px; width: 100%;">
			<tr>
                <td colspan="20" class="section-title">Metalliic Auto Liners Pvt. Lmt.</td>
			</tr>
            <tr>
                <td colspan="20" class="text-left">Aji Industries Estate, Gidc-ii Plot no.330<br /></td>
			</tr>
            <tr>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td colspan="7" class="text-left">Category : <b>DIRECTOR</b></td>
                <td colspan="6" class="text-left">P.F No. : GJ/16047</td>
                <td colspan="7" class="text-left">ESI No. : 37000023460000599</td>
			</tr>
		</table>
		<table class="table-company-detail" border="1" style="font-size: 13px; width: 100%;">
            <tr>
                <td class="text-left no-border-right" colspan="6">&nbsp; &nbsp;1</td>
                <td class="text-center no-border-right no-border-left" colspan="7"><b>Salary / Wage - Slip</b></td>
                <td class="text-right no-border-left" colspan="7">(From IV B [ Rule 26 (2) ])</td>
			</tr>
            <tr>
                <td class="text-left no-border-right no-border-bottom" colspan="10" ><b>Name  : JAYSHUKHLAL DHIRAJLAL JASANI</b></td>
                <td class="text-left no-border-right no-border-left no-border-bottom" colspan="4">Desg:</td>
                <td class="text-right no-border-left no-border-bottom" colspan="6"><b>Month : Jan - 2018</b></td>
            </tr>
            <tr>
                <td class="text-left no-border-right no-border-top no-border-top" colspan="3">Act Wage : 200000.00</td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="5">Mini Wage : 8387.60</td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="4">PF No. : GJ/16047</td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="4">UAN : </td>
                <td class="text-left no-border-left no-border-top" colspan="4">ESI No. : </td>
            </tr>
            <tr>
                <td class="text-right no-border-right no-border-bottom" colspan="2" ><b>Pr. Days</b></td>
                <td class="text-right no-border-right no-border-left no-border-bottom" colspan="2"><b>Wages</b></td>
                <td class="text-left no-border-right no-border-left no-border-bottom" colspan="2"><b>INCENTI</b></td>
                <td class="text-left no-border-right no-border-left no-border-bottom" colspan="2"><b>HRA</b></td>
                <td class="text-left no-border-left no-border-bottom" colspan="2"><b>CONV.</b></td>
                <td class="text-right no-border-right no-border-bottom" colspan="2"><b>P.F.</b></td>
                <td class="text-right no-border-right no-border-left no-border-bottom" colspan="2"><b>E.S.I.</b></td>
                <td class="text-right no-border-right no-border-left no-border-bottom" colspan="2"><b>P.T.</b></td>
                <td class="text-right no-border-right no-border-left no-border-bottom" colspan="2"><b>W.F.</b></td>
                <td class="text-left no-border-left no-border-bottom" colspan="2"><b>ADVANCE</b></td>
            </tr>
            <tr>
                <td class="text-right no-border-right no-border-top no-border-bottom" colspan="2">25.00</td>
                <td class="text-right no-border-right no-border-left no-border-top no-border-bottom" colspan="2">200000.00</td>
                <td class="text-left no-border-right no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-left no-border-right no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-left no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-right no-border-right no-border-top no-border-bottom" colspan="2">0</td>
                <td class="text-right no-border-right no-border-left no-border-top no-border-bottom" colspan="2">0.00</td>
                <td class="text-right no-border-right no-border-left no-border-top no-border-bottom" colspan="2">200.00</td>
                <td class="text-right no-border-right no-border-left no-border-top no-border-bottom" colspan="2">0.00</td>
                <td class="text-left no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td><br />
            </tr>
            <tr>
                <td class="text-right no-border-right no-border-top no-border-bottom" colspan="2"><b>SP.</b></td>
                <td class="text-left no-border-right no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-left no-border-right no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-left no-border-right no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-left no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-left no-border-right no-border-top no-border-bottom" colspan="2"><b>LOAN</b></td>
                <td class="text-left no-border-right no-border-left no-border-top no-border-bottom" colspan="2"><b>MOB.</b></td>
                <td class="text-right no-border-right no-border-left no-border-top no-border-bottom" colspan="2"><b>TDS/IT</b></td>
                <td class="text-left no-border-right no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td>
                <td class="text-left no-border-left no-border-top no-border-bottom" colspan="2">&nbsp;</td><br />
            </tr>
            <tr>
                <td class="text-center no-border-right no-border-top" colspan="2"><b>&nbsp;</b></td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="2">&nbsp;</td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="2">&nbsp;</td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="2">&nbsp;</td>
                <td class="text-left no-border-left no-border-top " colspan="2">&nbsp;</td>
                <td class="text-left no-border-right no-border-top" colspan="2"><b>&nbsp;</b></td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="2"><b>&nbsp;</b></td>
                <td class="text-right no-border-right no-border-left no-border-top" colspan="2">60000.00</td>
                <td class="text-left no-border-right no-border-left no-border-top" colspan="2">&nbsp;</td>
                <td class="text-left no-border-left no-border-top " colspan="2">&nbsp;</td><br />
            </tr>
            <tr>
                <td class="text-center" colspan="5">Gross :    200000.00</td>
                <td class="text-center" colspan="8">Deduction :    60200.00</td>
                <td class="text-center" colspan="7">Net Salary :    139800.00</td>
            </tr>
            <tr>
                <td class="text-center no-border-right line" colspan="7" rowspan="3"><br /><br/>Employee Sign&nbsp;</td>
                <td class="text-center no-border-right no-border-left line" colspan="6" rowspan="3"><br /><br />&nbsp;</td>
                <td class="text-center no-border-left line" colspan="7" rowspan="3"><br /><br />Authorised Sign</td>
            </tr>
		</table>
	</body>
</html>