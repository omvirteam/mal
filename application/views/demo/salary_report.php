<?php $this->load->view('success_false_notify'); ?>
<!--<style>div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }</style>-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Salary Report
        </h1>
    </section>
    <div class="clearfix">
        <div class="row">
            <div style="margin: 15px;">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-sm-5 input-sm">Select Month</label>
                                                <div class="col-sm-7">
                                                    <input type="text" id="month_year">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-sm-5 input-sm">Work Days</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="work_days" id="work_days" class="form-control input-sm num-only" value="">  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="box-body table-responsive">
                                        <table id="total_salary_table" class="table row-border order-column table-bordered table-striped cell-border" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No.</th>
                                                    <th>Employee Code</th>
                                                    <th>Employee Name</th>
                                                    <th>Present Days</th>
                                                    <th>Basic Salary</th>
                                                    <th>Basic Wage</th>
                                                    <th>HRA</th>
                                                    <th>Conveyance</th>
                                                    <th>Special Allowance</th>
                                                    <th>Total Salary</th>
                                                    <th>Overtime Hours</th>
                                                    <th>Overtime Amount</th>
                                                    <th>Other Pay 1</th>
                                                    <th>Other Pay 2</th>
                                                    <th>Earn Leave Salary</th>
                                                    <th>Bonus</th>
                                                    <th>Gratuity</th>
                                                    <th>PF @12% on Basic</th>
                                                    <th>ESCIC @1.75% On Total Salary</th>
                                                    <th>ESCIC @1.75% On Overtime</th>
                                                    <th>Professional Tax On Total Salary</th>
                                                    <th>Gujarat Labour Welfare Fund</th>
                                                    <th>TDS on Salary</th>
                                                    <th>Loan Deduction</th>
                                                    <th>Other Deduction 1</th>
                                                    <th>Other Deduction 2</th>
                                                    <th>Net Basic Salary</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>1234</td>
                                                    <td>Ramji Bhai</td>
                                                    <td>28</td>
                                                    <td>5000</td>
                                                    <td>50</td>
                                                    <td>50</td>
                                                    <td>50</td>
                                                    <td>50</td>
                                                    <td>5200</td>
                                                    <td>8</td>
                                                    <td>50</td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td>600</td>
                                                    <td>88</td>
                                                    <td>50</td>
                                                    <td>0</td>
                                                    <td>6</td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td>6000</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2134</td>
                                                    <td>Paresh Bhai</td>
                                                    <td>27</td>
                                                    <td>6000</td>
                                                    <td>50</td>
                                                    <td>50</td>
                                                    <td>50</td>
                                                    <td>50</td>
                                                    <td>6200</td>
                                                    <td>8</td>
                                                    <td>50</td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td>700</td>
                                                    <td>88</td>
                                                    <td>50</td>
                                                    <td>0</td>
                                                    <td>6</td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td><input type="text" value="50"></td>
                                                    <td>6500</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        table = $('#total_salary_table').DataTable({
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
        });
        

$('#month_year').datepicker({
    format: "mm-yyyy",
});
    });
</script>


