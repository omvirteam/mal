<?php $this->load->view('success_false_notify'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Department Wise Attendance
        </h1>
    </section>
    <div class="clearfix">
        <div class="row">
            <div style="margin: 15px;">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-body">
							<div class="row">								
								<div class="col-md-8">									
										<label class="col-md-2"> Department :</label>
										<div class="col-md-4">										
											 <select class="form-control input-sm">
												 <option value="1">--Select Department--</option>
												<option value="1">IT</option>
												<option value="2">Production</option>
												<option value="3">Marketing</option>
												<option value="3">Management</option>
											</select>
										 </div>
										<label class="col-md-2"> Date :</label>
										<div class="col-md-4">
											<input type="text" name="date" id="datepicker1" class="form-control input-sm input-datepicker">
										</div>
                                 </div>
							</div>	
							<br>					
                            <div class="row">
								 <div class="col-md-6">
									<table class="table table-bordered ">
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Present</th>                        
											</tr>
										</thead>
										<tbody class="text-white bg-green">
											<tr>
												<td>1</td>
												<td>Employee Name1</td>
											</tr>
											<tr>
												<td>2</td>
												<td>Employee Name3</td>
											</tr>
											<tr>
												<td>3</td>
												<td>Employee Name4</td>
											</tr>
											<tr>
												<td>4</td>
												<td>Employee Name5</td>
											</tr>
											<tr>
												<td>5</td>
												<td>Employee Name6</td>
											</tr>
											<tr>
												<td>6</td>
												<td>Employee Name7</td>
											</tr>
											<tr>
												<td>7</td>
												<td>Employee Name9</td>
											</tr>
										</tbody>
									</table>							 
								 
								 </div>
								 <div class="col-md-6">
									<table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No.</th>
                                                    <th>Absent</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody class="text-white bg-red">
												<tr>
													<td>1</td>
													<td >Employee Name2</td>
												</tr>
												<tr>
													<td>2</td>
													<td>Employee Name8</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Employee Name10</td>
												</tr>
                                            </tbody>
                                        </table>							
								</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


