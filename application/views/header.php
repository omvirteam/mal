<?php
if(isset($this->session->userdata()['mal_is_logged_in']) && !empty($this->session->userdata()['mal_is_logged_in'])){
	$logged_in_name = $this->session->userdata()['mal_is_logged_in']['admin_name'];
	$logged_in_email = $this->session->userdata()['mal_is_logged_in']['admin_email_id'];
	$logged_in_image = ''; //$this->session->userdata()['mal_is_logged_in']['user_img'];
}
$currUrl = $this->uri->segment(1);
if($currUrl == ''){
	$currUrl = 'Dashboard';
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title><?=ucwords($currUrl)?> | Mal</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?=base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?=base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">		
		<link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/skin-blue.min.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/dist/css/custom.css">
		<!-- jvectormap -->
		<link rel="stylesheet" href="<?=base_url()?>assets/bower_components/jvectormap/jquery-jvectormap.css">
		<!-- Date Picker -->
		<link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
		<!-- Daterange picker -->
		<link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
		<!-- bootstrap wysihtml5 - text editor -->
		<link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
		
		<!-- iCheck for checkboxes and radio inputs -->
		<link rel="stylesheet" href="<?= base_url();?>assets/plugins/iCheck/all.css">
		<!----------------Notify---------------->
		<link rel="stylesheet" href="<?=base_url('assets/plugins/notify/jquery.growl.css');?>">
		<!-------- /Parsleyjs --------->
		<link href="<?= base_url('assets/plugins/parsleyjs/src/parsley.css');?>" rel="stylesheet" type="text/css" />
		
        <!-- DataTables -->
        <link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/media/css/jquery.dataTables.min.css');?>">
        <link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/extensions/Scroller/css/scroller.dataTables.min.css');?>">
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datatables/extensions/FixedColumns/css/fixedColumns.dataTables.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

		<![endif]-->
		<!-- Google Font -->
		<!-- REQUIRED JS SCRIPTS -->
		<!-- jQuery 3 -->
		<script src="<?=base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<!-- AdminLTE App -->
		<script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>		
		<script src="<?=base_url()?>assets/dist/js/app.js"></script>
		<!-- jQuery UI 1.11.4 -->
		<script src="<?=base_url()?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
		
		<link rel="stylesheet"
			  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<!-- Main Header -->
			<header class="main-header">
				<!-- Logo -->
				<a href="<?=base_url();?>" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>M</b></span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>M</b>al</span>
				</a>
				<!-- Header Navbar -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<!-- Navbar Right Menu -->
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account Menu -->
							<li class="dropdown user user-menu">
								<!-- Menu Toggle Button -->
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<!-- The user image in the navbar-->
									<img src="<?=base_url()?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
									<!-- hidden-xs hides the username on small devices so only the image appears. -->
									<span class="hidden-xs"><?=isset($logged_in_name)?ucwords($logged_in_name):'Admin';?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- The user image in the menu -->
									<li class="user-header">
										<img src="<?=base_url()?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
										<p>
											<?=isset($logged_in_name)?ucwords($logged_in_name):'Admin';?>
											<small><?=isset($logged_in_email)?$logged_in_email:'';?></small>
										</p>
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="<?=base_url()?>auth/profile" class="btn btn-default btn-flat">Change Password</a>
										</div>
										<div class="pull-right">
											<a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
										</div>
									</li>
								</ul>
							</li>							
						</ul>
					</div>
				</nav>
			</header>