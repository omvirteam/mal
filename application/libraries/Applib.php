<?php

/**
 * Class AppLib
 * &@property CI_Controller $ci
 */
class AppLib
{
    function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();
        $this->ci->load->library('session');
        //$data = $this->ci->session->userdata('mal_is_logged_in');
    }

    /***
     * @param $file_url
     * @return bool
     */
    function unlink_file($file_url)
    {
        if (file_exists($file_url)) {
            if (unlink($file_url)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $image
     * @param $upload_url
     * @param bool $return_with_url
     * @return bool|string
     */
    function upload_image($file, $upload_url, $return_with_url = true){
		$config['upload_path'] = $upload_url;
		$config['allowed_types'] = '*';
        $config['encrypt_name'] = TRUE;
		$this->ci->load->library('upload', $config);
        $this->ci->upload->initialize($config);
        if (!$this->ci->upload->do_upload($file)) {
			print_r($this->ci->upload->display_errors());exit;
            $error = array('error' => $this->upload->display_errors());
            return false;
        }
        $data = $this->ci->upload->data();
        $file_name = $data ['file_name'];
        if ($return_with_url) {
            return $upload_url . $file_name;
        } else {
            return $file_name;
        }

    }

    /**
     * @param string $date
     * @return bool|string
     * DD/MM/YYYY To YYYY-MM-DD
     */
    function to_sql_date($date = '')
    {
        return date('Y-m-d', strtotime($date));
    }

    /**
     * @param string $date
     * @return bool|string
     * YYYY-MM-DD To DD/MM/YYYY
     */
    function to_simple_date($date = '')
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $date)));
    }

    /**
     * @param $SearchResults
     * @param $SortResults
     * @return array
     */
    function order_search($SearchResults, $SortResults){
        return array_values(array_intersect($SearchResults, $SortResults));
    }

}
