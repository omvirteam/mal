-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 27, 2018 at 11:39 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(222) DEFAULT NULL,
  `admin_phone` varchar(22) DEFAULT NULL,
  `admin_email_id` varchar(50) DEFAULT NULL,
  `admin_password` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_phone`, `admin_email_id`, `admin_password`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Main Admin', '7676767676', 'admin@gmail.com', '75d23af433e0cea4c0e45a56dba18b30', 1, '2018-01-16 00:00:00', 1, '2018-01-16 00:00:00');

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;



-- Ishita : 2018_05_26 11:30 AM
--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL,
  `emp_name` varchar(255) DEFAULT NULL,
  `dept` varchar(255) DEFAULT NULL,
  `pf_applicable` varchar(255) DEFAULT NULL,
  `esic_applicable` varchar(255) DEFAULT NULL,
  `basic_salary` int(11) DEFAULT NULL,
  `basic_wage` int(11) DEFAULT NULL,
  `hra` int(11) DEFAULT NULL,
  `pay_type` varchar(255) DEFAULT NULL,
  `conveyance` int(11) DEFAULT NULL,
  `special_allowance` int(11) DEFAULT NULL,
  `total_salary` int(11) DEFAULT NULL,
  `earn_leave_salary` int(11) DEFAULT NULL,
  `bonus` int(11) DEFAULT NULL,
  `gratuity` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT;